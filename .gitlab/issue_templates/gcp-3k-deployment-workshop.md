:exclamation: - In the Title field of this issue, use `<Your Name> - <Your Company> - <Date>`

# Deploying GitLab Reference Architecture on GCP with GET

**IMPORTANT:** This workshop assumes that you are using GET 2.0.1 or newer. (GET = GitLab Environment Toolkit)

## Workshop pre-work

*This should be completed before the SKO breakout session*

If you are a GitLab Team member, be sure to use [GitLab Sandbox Cloud](https://gitlabsandbox.cloud/) to [create a GCP project](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#individual-aws-account-or-gcp-project) for you.

GET uses Terraform to provision Infrastructure and Ansible to manage GitLab configuration; 
It is possible to use [Terraform](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_provision.md#install-terraform-using-asdf) and [Ansible](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_configure.md#1-install-ansible) installed locally; However, during this session, to avoid potential environmental issues, we are going to use [Toolkit's image](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/container_registry/2697240). To do so we need to prepare our environment by installing Docker and Git. Our suggestion is to deploy a GCP VM instance in the region you have chosen to run this workshop. The instance OS is a suggestion but the Docker installation might be different if you pick a different OS. Check the official Docker [documentation](https://docs.docker.com/engine/install).

> The instructions that follow are prescriptive of an amd64 Mac.

1. - [ ] Access the `GCP Console`
      - [ ] Choose a region and add it to the next line (e.g. us-central1):
        - [ ] GCP Region: __________
      - [ ] Create a GCP service account key. Save these keys locally: `GOOGLE_APPLICATION_CREDENTIALS` to later use;
      - [ ] Choose a `prefix` (gitlab-YOURGITLABHANDLE-3k for example): _______
      - [ ] Access Compute Engine
         - [ ] Create a new VM instance
         - [ ] Allocate an external IP and add it to the lines below:
         - [ ] External IP: __________

1. - [ ] Launch a `n2-standard-2` instance with `Ubuntu 20.04 LTS` selecting the key created previously. _Note: the costs of deploying GitLab in this workshop are the responsibility of the workshop student or student's company._
      - [ ] Make sure you have Firewall rules allowing `SSH TCP 22`
      - [ ] Access your instance by selecting from the VM instances listing your Instance ID, followed by selecting SSH
      - [ ] Install Docker by running in the terminal you connected to the following commands:
        - [ ] `sudo apt-get update -y`
        - [ ] `sudo apt-get install -y docker.io`
        - [ ] `sudo systemctl enable docker`
        - [ ] `sudo systemctl start docker`
        - [ ] `sudo usermod -aG docker $USER`
      - [ ] Run `sudo apt-get install -y git`
1. - [ ] On the same instance terminal run the next steps:
      - [ ] Run `cd /home/$USER`
      - [ ] Run `git clone https://gitlab.com/gitlab-org/gitlab-environment-toolkit.git` to clone GET
      - [ ] Run `sudo docker pull registry.gitlab.com/gitlab-org/gitlab-environment-toolkit:latest` to pull the Toolkit's image
        - If you got **Getting permission denied while trying to connect to the Docker daemon socket** just restart the instance, if the issue persists you can run `sudo chmod 666 /var/run/docker.sock` ssh on it again.
      - [ ] Run `cd gitlab-environment-toolkit` to access the keys directory
      - [ ] Run `ssh-keygen -t rsa -b 2048 -f keys/id_rsa` leaving the passphrase empty, and completing the ssh key creation (these will be used on the gitlab instance)

## Workshop work

*During the session*

### Terraform Configuration

1. - [ ] Configure GET to deploy the [3K reference architecture](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html). Open a terminal and run `cd /home/$USER/gitlab-environment-toolkit` cloned previously, run the steps that follow:
     - [ ] run `mkdir terraform/environments/3k`
     - [ ] run `cd terraform/environments/3k`
     - [ ] run `touch variables.tf main.tf environment.tf`

The directory structure should look like below:

```sh
gitlab-environment-toolkit
    └── terraform
        └── environments
                └── 3k
                    ├── variables.tf
                    └── main.tf
                    └── environment.tf
```

Following the [documentation](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_provision.md#google-cloud-platform-gcp) we are going to edit the 3 files created in the previous step:

1. - [ ] In the `variables.tf` file replace the variables accordingly.
      - [ ] Replace `prefix` value
      - [ ] Replace `region` value
      - [ ] Replace `external_ip`
      - [ ] Replace `ssh_public_key_file` with your key location (if the pre-work instructions were followed, the location is `"../../../keys/id_rsa.pub"`)

Both the public key and the external IP were provisioned as part of workshop pre-work (beginning of this tutorial) as well as the `prefix`. This `prefix` variable later is going to be used on our Ansible configuration. Your `variables.tf` file should look like the following:

```tf
variable "prefix" {
    default = "gitlab-afonseca-3k"
}

variable "region" {
    default = "us-central1"
}

variable "ssh_public_key_file" {
    default = "../../../keys/id_rsa.pub"
}

variable "external_ip" {
    default = "34.68.194.64" 
}
```

We use local storage of the Terraform state for this workshop. Alternatively, you can [store](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_provision.md#L168-171) it in a previously provisioned bucket.

1. - [ ] The file `main.tf` should look like below:

```tf
terraform {
    required_providers {
        google = {
        source = "hashicorp/google"
        }
    }
}

# Configure the GCP Provider
provider "google" {
    project = var.gcp_project_id
    region  = var.region
}
```

Edit the `environment.tf` file to configure Terraform with the target [Reference Architecture](https://docs.gitlab.com/ee/administration/reference_architectures/). During this workshop, we are going to use [created network mode](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_advanced_network.md#configure-network-setup-gcp) with 3 subnets. GET is quite flexible and the network configuration can be customized to your needs. For more information check the Advanced Network [documentation](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_advanced_network.md).

1. - [ ] For the [3K Architecture](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html) this file should look like follows:

```tf
module "gitlab_ref_arch_gcp" {
    source = "../../modules/gitlab_ref_arch_gcp"

    prefix              = var.prefix
    ssh_public_key      = file(var.ssh_public_key_file)

    # using network mode create with 3 subnets
    create_network = true
    subnet_count = 3

    # External load balancer node
    haproxy_external_node_count = 1
    haproxy_external_machine_type = "n2-standard-2"

    # Redis
    redis_node_count = 3
    redis_machine_type = "n2-standard-2"
    
    # Consul + Sentinel
    consul_node_count = 3
    consul_machine_type = "n2-standard-2"

    # Postgres
    postgres_node_count = 3
    postgres_machine_type = "n2-standard-2"

    # Pgbouncer
    pgbouncer_node_count = 3
    pgbouncer_machine_type = "n2-standard-2"

    # External Load Balancer
    haproxy_external_ip = var.external_ip
    
    # Internal Load balancer node
    haproxy_internal_node_count = 1
    haproxy_internal_machine_type = "n2-standard-2"
    
    # gitaly
    gitaly_node_count = 3
    gitaly_machine_type = "n2-standard-4"
    
    # praefect
    praefect_node_count = 3
    praefect_machine_type = "n2-standard-2"

    # praefect postgres
    praefect_postgres_node_count = 1
    praefect_postgres_machine_type = "n2-standard-2"

    # nfs
    gitlab_nfs_node_count = 1
    gitlab_nfs_machine_type = "n2-standard-4"

    #rails   
    gitlab_rails_node_count = 3
    gitlab_rails_machine_type = "n2-standard-8"
    
    #grafana and prometheus
    monitor_node_count = 1
    monitor_machine_type = "n2-standard-2"
    
    #sidekiq
    sidekiq_node_count = 4
    sidekiq_machine_type = "n2-standard-2"
}
```

### Ansible configuration

Now let's configure Ansible, first creating the required directory structure and files.

1. - [ ] In a terminal from the directory `gitlab-environment-toolkit` execute the following steps:
      - [ ] Run `mkdir -p ansible/environments/3k/inventory`
      - [ ] Run `cd ansible/environments/3k/inventory`
      - [ ] Run `touch 3k.gcp.yml vars.yml`

The directory structure should look like below:

```sh
gitlab-environment-toolkit
    └── ansible
        └── environments
            └── 3k
                └── inventory
                      ├── 3k.gcp.yml
                      └── vars.yaml 
```

1. - [ ] Now lets configure the GCP Dynamic Inventory plugin. Edit `3k.gcp.yml` to look similar to the following:
     - [ ] Replace `project_id` accordingly
     - [ ] Replace `zone` accordingly
     - [ ] Replace `prefix` accordingly
     - [ ] Don't change `ansible_host`. The value `networkInterfaces[0].accessConfigs[0].natIP` is the correct value.

```yml
plugin: gcp_compute
projects:
  - your-project-id
zones:
  - us-central1-a
filters:
  - name = "gitlab-afonseca-3k-*"
keyed_groups:
  - key: labels.gitlab_node_type
    separator: ''
  - key: labels.gitlab_node_level
    separator: ''
hostnames:
  - name
compose:
  ansible_host: networkInterfaces[0].accessConfigs[0].natIP
```

> Now we need to provide the variables to the Ansible playbooks, for simplicity, we are going to use just one Password across the application, but this can be configured as per your needs. We are also avoiding passwords in the playbooks using environment variables through `lookup('env',...)` that pulls the password for us. In the next section, the variable `GITLAB_PASSWORD` will be defined.

1. - [ ] Set the variables accordingly to produce a `vars.yml` similar to the following
     - [ ] Replace `prefix`
     - [ ] Replace `external_url` with the allocated external IP
     - [ ] Uncomment and add your `gitlab_license_file` if it's available

```yml
all:
  vars:
    # Ansible Settings
    ansible_user: $USER
    ansible_ssh_private_key_file: "../keys/id_rsa"

    # Cloud Settings, available options: gcp, aws, azure
    cloud_provider: "gcp"

    # General Settings
    prefix: "gitlab-afonseca-3k"
    external_url: "http://34.68.194.64"
    # gitlab_license_file: "../../../sensitive/GitLabBV.gitlab-license"

    # Component Settings
    patroni_remove_data_directory_on_rewind_failure: false
    patroni_remove_data_directory_on_diverged_timelines: false

    # Passwords / Secrets
    gitlab_root_password: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    grafana_password: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    postgres_password: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    patroni_password: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    consul_database_password: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    gitaly_token: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    pgbouncer_password: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    redis_password: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    praefect_external_token: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    praefect_internal_token: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    praefect_postgres_password: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
```

### Running Terraform and Ansible from Toolkit's Container

1. - [ ] Open a terminal and set the following environment variables for the Terraform module: `GOOGLE_APPLICATION_CREDENTIALS` and `GITLAB_PASSWORD`, using for the GitLab root password something generated from a password generator.
     - [ ] Run, `export GOOGLE_APPLICATION_CREDENTIALS=<path_to_your_service_account_key>`
     - [ ] Run, `export GITLAB_PASSWORD=<GITLAB_PASSWORD>`

> To generate your own password use a [password generator](https://www.passwordsgenerators.net/), uncheck `Include Symbols:`, and check `Exclude Ambiguous Characters` to avoid issues with password polices across architecture components. The requirements below generally work fine:

![Password Requirements](img/password_requirements.png)

1. - [ ] Configure [Ansible output logging](https://docs.ansible.com/ansible/latest/reference_appendices/logging.html#logging-ansible-output)
     - [ ] Open a terminal edit `gitlab-environment-toolkit/ansible/ansible.cfg` adding the following lines under `[defaults]`:
```ini
    log_path = /gitlab-environment-toolkit/ansible/environments/3k/inventory/ansible.log
    display_args_to_stdout = True
```

1. - [ ] Run a Toolkit Docker Container in the interactive passing the environment variables and mounting the required volume folders.
      First, take a note of the full path to the folders that follows:
     - [ ] `gitlab-environment-toolkit/keys`
     - [ ] `gitlab-environment-toolkit/ansible/environments/3k`
     - [ ] `gitlab-environment-toolkit/ansible/ansible.cfg`
     - [ ] `gitlab-environment-toolkit/terraform/environments/3k`
     - [ ] Replace the following markers with the full path in the Docker command below:
        - [ ] _<host_full_path_to_keys>_
        - [ ] _<host_full_path_to_ansible_environment_3k>_
        - [ ] _<host_full_path_to_ansible.cfg_file>_
        - [ ] _<host_path_to_terraform_environment_3k>_ 

```sh
docker run -it \
-v <host_full_path_to_keys>:/gitlab-environment-toolkit/keys \
-v <host_full_path_to_ansible_environment_3k>:/gitlab-environment-toolkit/ansible/environments/3k \
-v <host_full_path_to_ansible.cfg_file>:/gitlab-environment-toolkit/ansible/ansible.cfg \
-v <host_path_to_terraform_environment_3k>:/gitlab-environment-toolkit/terraform/environments/3k \
-e GOOGLE_APPLICATION_CREDENTIALS=$GOOGLE_APPLICATION_CREDENTIALS \
-e GITLAB_PASSWORD=$GITLAB_PASSWORD \
registry.gitlab.com/gitlab-org/gitlab-environment-toolkit:latest
```

In my instance, the paths looks like this:

```sh
docker run -it  \
-v /home/$USER/gitlab-environment-toolkit/keys:/gitlab-environment-toolkit/keys \
-v /home/$USER/gitlab-environment-toolkit/ansible/environments/3k:/gitlab-environment-toolkit/ansible/environments/3k \
-v /home/$USER/gitlab-environment-toolkit/ansible/ansible.cfg:/gitlab-environment-toolkit/ansible/ansible.cfg \
-v /home/$USER/gitlab-environment-toolkit/terraform/environments/3k:/gitlab-environment-toolkit/terraform/environments/3k \
-e GOOGLE_APPLICATION_CREDENTIALS=$GOOGLE_APPLICATION_CREDENTIALS \
-e GITLAB_PASSWORD=$GITLAB_PASSWORD \
registry.gitlab.com/gitlab-org/gitlab-environment-toolkit:latest
```

1. - [ ] Run the Terraform sequence to provision the required infrastructure from within the container
     - [ ] Install Terraform in the container with `mise install terraform -y`
     - [ ] Run `cd /gitlab-environment-toolkit/terraform/environments/3k`
     - [ ] Run `terraform init`
     - [ ] Run `terraform plan -out 3k.gcp.tfplan`
     - [ ] Run `terraform apply 3k.gcp.tfplan`
       - (w/o "plan") Enter 'yes' to perform the actions

2. - [ ] Inside the container let's run the Ansible scripts
      - [ ] Run `cd /gitlab-environment-toolkit/ansible`
      - [ ] To validate Ansible configuration run `ansible all -m ping -i environments/3k/inventory --list-hosts`
      - [ ] Run `ansible-playbook -i environments/3k/inventory playbooks/all.yml`
         - **NOTE:** This might take up to an hour to complete
      - [ ] Once the process is complete, with no `ERROR`s, you may exit the container by typing `exit` (or via `Ctrl+D`)

### Testing the GitLab Deployment

1. - [ ] Back to the instance terminal lets ssh in a rails node to check gitlab status and puma logs.
     - [ ] Running something similar to `ssh -i /home/$USER/gitlab-environment-toolkit/keys/id_rsa $USER@your-rails-node-public-dns`
     - [ ] Run `sudo gitlab-ctl status`
     - [ ] Run `sudo gitlab-ctl tail puma`
     - [ ] Access the GitLab application using `http://<your_external_ip>` (same as provided on `external_url` in `vars.yaml`)
        - [ ] Login with user `root` and the password as set in the variable `GITLAB_PASSWORD`
        - [ ] Create an issue in a new project, including the default `README.md` file
        - [ ] Create a merge request (MR) from the issue and change the project's `README.md` by adding as title `Congratulations! You deployed GitLab HA with GET`
        - [ ] Commit change and merge the MR

## Post Screenshots (for certification only)

1. - [ ] Provide screenshots of your GCP console showing the various compute nodes that were provisioned during this workshop.
1. - [ ] Provide screenshots of the running instance of GitLab (include the url bar with matching IP address to your `external ip` from initial steps) as a comment in this issue.
1. - [ ] Attach the GET (Ansible) logs as a comment to this issue. You can copy it locally to attach to the issue. Running a command similar to `scp -i "/path/to/key" $USER@your-public-dns:/path/to/ansible.log /local/path/to/ansible.log`
1. - [ ] Return to LevelUp to provide a link to this issue and your user ID. We will administer the grade for this workshop in LevelUp

## Next Steps

### Complete the workshop

**NOTE:** Please don't forget to destroy this environment using Terraform once you are done using it. You can do that from the container running:

- [ ] `cd /gitlab-environment-toolkit/terraform/environments/3k`
- [ ] `terraform destroy`
- [ ] To terminate the GET instance, through the GCP console, stop and delete the instance.

### Continue to use cloud managed Services for stateful components

- [ ] Create [an issue](https://gitlab.com/gitlab-org/professional-services-automation/tools/implementation/GET-deployment-workshop/-/issues/new?issuable_template=gcp-3k-cloud-managed-services) to follow how to replace and use cloud managed services like Cloud SQL and Memorystore with GET.

/label ~"workshop::doing"

That is everything for today. Thank you so much for your attendance!
